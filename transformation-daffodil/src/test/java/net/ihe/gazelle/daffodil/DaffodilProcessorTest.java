package net.ihe.gazelle.daffodil;

import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.TransformationException;
import org.custommonkey.xmlunit.Diff;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

/**
 * Created by cquere on 14/03/17.
 */
public class DaffodilProcessorTest {

    private static final String RESOURCE_FOLDER = "src/test/resources/";

    @Test
    public void transformBinaryDataToXMLNominalTest() throws IOException, CompilationException, TransformationException, SAXException {
        File schemaFile = new File(RESOURCE_FOLDER + "twlab/mms_mmxu.dfdl.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        byte[] inData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/payload_157.asn1"));
        String producedXml = daffodilProcessor.transformDataToXML(inData);

        String expectedXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/payloadTransformed.xml")), StandardCharsets.UTF_8);
        Diff myDiff = new Diff(expectedXml, producedXml);
        assertTrue("Transformed data must be similar to the expected result " + myDiff, myDiff.similar());
        assertTrue("Transformed data must be identical to the expected result " + myDiff, myDiff.identical());
    }

    @Test
    public void transformTextDataToXMLNominalTest() throws IOException, CompilationException, TransformationException, SAXException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        byte[] inData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "rsp580/valid_cs.dat"));
        String producedXml = daffodilProcessor.transformDataToXML(inData);

        String expectedXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "rsp580/valid_cs.xml")), StandardCharsets.UTF_8);
        Diff myDiff = new Diff(expectedXml, producedXml);
        assertTrue("Transformed data must be similar to the expected result " + myDiff, myDiff.similar());
        assertTrue("Transformed data must be identical to the expected result " + myDiff, myDiff.identical());
    }

    @Test
    public void transformTextDataToXMLNominalTestEFS() throws IOException, CompilationException, TransformationException, SAXException {
        File schemaFile = new File(RESOURCE_FOLDER + "NF_S97_2010/dfdl/02_Delivrance.dfdl.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        byte[] inData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "NF_S97_2010/Delivrance_OK.txt"));
        String producedXml = daffodilProcessor.transformDataToXML(inData);

        String expectedXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "NF_S97_2010/Delivrance_OK.xml")), StandardCharsets.UTF_8);
        Diff myDiff = new Diff(expectedXml, producedXml);
        assertTrue("Transformed data must be similar to the expected result " + myDiff, myDiff.similar());
        assertTrue("Transformed data must be identical to the expected result " + myDiff, myDiff.identical());
    }

    @Test(expected = TransformationException.class)
    public void transformNullDataToXmlErrorTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        byte[] inData = new byte[0];
        daffodilProcessor.transformDataToXML(inData);
    }

    @Test(expected = TransformationException.class)
    public void tranformTextDataToXmlErrorTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        byte[] inData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "rsp580/invalid_cs.dat"));
        daffodilProcessor.transformDataToXML(inData);
    }

    @Test(expected = TransformationException.class)
    public void tranformBinaryDataToXmlErrorTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "twlab/mms_mmxu.dfdl.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        byte[] inData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/invalid_payload_157.asn1"));
        daffodilProcessor.transformDataToXML(inData);
    }

    @Test
    public void transformXmlToTextDataNominalTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        String inXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "rsp580/valid_cs.xml")), StandardCharsets.UTF_8);
        byte[] producedData = daffodilProcessor.transformXMLToData(inXml);

        byte[] expectedData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "rsp580/valid_cs.dat"));
        Assert.assertArrayEquals("Transformed XML must be equals to the expeced result", expectedData, producedData);
    }

    @Test
    public void transformXmlToBinaryDataNominalTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "twlab/mms_mmxu.dfdl.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        String inXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/payloadTransformed.xml")), StandardCharsets.UTF_8);
        byte[] producedData = daffodilProcessor.transformXMLToData(inXml);

        byte[] expectedData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/payload_157.asn1"));
        Assert.assertArrayEquals("Transformed XML must be equals to the expeced result", expectedData, producedData);
    }

    @Test(expected = TransformationException.class)
    public void transformXmlToTextDataErrorTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        String inXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "rsp580/invalid_cs.xml")), StandardCharsets.UTF_8);
        daffodilProcessor.transformXMLToData(inXml);
    }

    @Test(expected = TransformationException.class)
    public void transformXmlToBinaryDataErrorTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "twlab/mms_mmxu.dfdl.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        String inXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/invalid_payloadTransformed.xml")), StandardCharsets.UTF_8);
        daffodilProcessor.transformXMLToData(inXml);
    }

    @Test(expected = TransformationException.class)
    public void transformNullXmlToDataErrorTest() throws IOException, CompilationException, TransformationException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        String inXml = "";
        daffodilProcessor.transformXMLToData(inXml);
    }

    @Test
    public void saveAndReloadTest() throws IOException, CompilationException, ProcessorReloadException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        File savedFile = Files.createTempFile("savedProcessor", ".tmp").toFile();
        daffodilProcessor.save(savedFile);
        daffodilProcessor = null;

        DaffodilProcessor reloadDaffodilProcessor = DaffodilProcessorFactory.reloadProcessor(savedFile);
        Assert.assertNotNull("Reloaded daffodil processor must not be null", reloadDaffodilProcessor);
    }

    @Test
    public void saveAndReloadWithTransformTest()
            throws IOException, CompilationException, ProcessorReloadException, TransformationException, SAXException {
        File schemaFile = new File(RESOURCE_FOLDER + "twlab/mms_mmxu.dfdl.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        File savedFile = Files.createTempFile("savedProcessor", ".tmp").toFile();
        daffodilProcessor.save(savedFile);
        daffodilProcessor = null;

        DaffodilProcessor reloadDaffodilProcessor = DaffodilProcessorFactory.reloadProcessor(savedFile);
        Assert.assertNotNull("Reloaded daffodil processor must not be null", reloadDaffodilProcessor);

        byte[] inData = Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/payload_157.asn1"));
        String producedXml = reloadDaffodilProcessor.transformDataToXML(inData);

        String expectedXml = new String(Files.readAllBytes(Paths.get(RESOURCE_FOLDER + "twlab/payloadTransformed.xml")), StandardCharsets.UTF_8);
        Diff myDiff = new Diff(expectedXml, producedXml);
        assertTrue("Transformed data must be similar to the expected result " + myDiff, myDiff.similar());
        assertTrue("Transformed data must be identical to the expected result " + myDiff, myDiff.identical());
    }

}