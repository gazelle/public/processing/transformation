package net.ihe.gazelle.daffodil;

import net.ihe.gazelle.transformation.service.CompilationException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Created by cel on 16/01/18
 */
public class DaffodilProcessorFactoryTest {

    private static final String RESOURCE_FOLDER = "src/test/resources/";

    @Test
    public void compileProcessorNominalTest() throws IOException, CompilationException {
        File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
        Assert.assertNotNull("Compiled daffodilProcessor must not be null", daffodilProcessor);
    }

    @Test(expected = CompilationException.class)
    public void compileProcessorInvalidTest() throws IOException, CompilationException {
        File schemaFile = new File(RESOURCE_FOLDER + "rsp580/invalid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.compileProcessor(schemaFile);
    }

    @Test
    public void reloadProcessorNominalTest() throws ProcessorReloadException {
        File processorFile = new File(RESOURCE_FOLDER + "save.test");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.reloadProcessor(processorFile);
        Assert.assertNotNull("Reloaded daffodilProcessor must not be null", daffodilProcessor);
    }

    @Test(expected = ProcessorReloadException.class)
    public void reloadProcessorErrorTest() throws ProcessorReloadException {
        File processorFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
        DaffodilProcessor daffodilProcessor = DaffodilProcessorFactory.reloadProcessor(processorFile);
    }

}
