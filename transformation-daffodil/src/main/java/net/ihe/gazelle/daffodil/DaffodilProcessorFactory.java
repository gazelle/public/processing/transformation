package net.ihe.gazelle.daffodil;

import edu.illinois.ncsa.daffodil.japi.Compiler;
import edu.illinois.ncsa.daffodil.japi.Daffodil;
import edu.illinois.ncsa.daffodil.japi.DataProcessor;
import edu.illinois.ncsa.daffodil.japi.InvalidParserException;
import edu.illinois.ncsa.daffodil.japi.InvalidUsageException;
import edu.illinois.ncsa.daffodil.japi.ProcessorFactory;
import edu.illinois.ncsa.daffodil.japi.ValidationMode;
import net.ihe.gazelle.transformation.service.CompilationException;

import java.io.File;
import java.io.IOException;


/**
 * Build Daffodil processor by compiling a DFDL schema or reloading it.
 *
 * @author ceoche
 */
public class DaffodilProcessorFactory {

    /**
     * Compile DFDL schema and build the associated {@link DaffodilProcessor}. Then use {@link DaffodilProcessor#transformDataToXML(byte[])} or
     * {@link DaffodilProcessor#transformXMLToData(String)} to parse/unparse.
     * <p>
     * The compiled processor can also be saved in a file invoking {@link DaffodilProcessor#save(File)}.
     *
     * @param xsdDfdlFile file containing the DFDL schema
     * @return a {@link DaffodilProcessor} ready for transformation with the given DFDL schema
     * @throws IOException          in case of file access error
     * @throws CompilationException in case of schema compilation error. Use {@link CompilationException#getMessage()} to get compilation details
     */
    public static DaffodilProcessor compileProcessor(File xsdDfdlFile) throws IOException, CompilationException {

        ProcessorFactory processorFactory = getFactory(xsdDfdlFile);
        if (!processorFactory.isError()) {
            DataProcessor dataProcessor = processorFactory.onPath("/");
            if (!dataProcessor.isError()) {
                try {
                    dataProcessor.setValidationMode(ValidationMode.Full);
                } catch (InvalidUsageException e) {
                    throw new CompilationException("Unable to set full-validation mode: " + e.getMessage(), e);
                }
                return new DaffodilProcessor(dataProcessor);
            } else {
                throw new CompilationException(DiagnosticUtil.extractDiagnostics(dataProcessor.getDiagnostics()));
            }
        } else {
            throw new CompilationException(DiagnosticUtil.extractDiagnostics(processorFactory.getDiagnostics()));
        }

    }

    /**
     * Load a {@link DaffodilProcessor} from a previously saved compilation.
     *
     * @param savedProcessorFile file containing the saved processor
     * @return a {@link DaffodilProcessor} ready for transformation
     * @throws ProcessorReloadException in case of trouble reloading the processor (file access error, corrupted object...)
     * @see DaffodilProcessor#save(File)
     */
    public static DaffodilProcessor reloadProcessor(File savedProcessorFile) throws ProcessorReloadException {
        try {
            return new DaffodilProcessor(getCompiler().reload(savedProcessorFile));
        } catch (InvalidParserException e) {
            throw new ProcessorReloadException("Unexpected error, unable to load pre-compiled processor: " + e.getMessage(), e);
        }
    }

    private static Compiler getCompiler() {
        Compiler compiler = Daffodil.compiler();
        compiler.setValidateDFDLSchemas(true);
        return compiler;
    }

    private static ProcessorFactory getFactory(File xsdDfdlFile) throws IOException, CompilationException {
        try {
            return getCompiler().compileFile(xsdDfdlFile);
        } catch (ClassCastException e) {
            // The error "cannot be cast to java.net.URLClassLoader" is thrown when an XSD inclusion failed to be resolved.
            // Translate it in a human friendly one!
            if (e.getMessage().contains("org.jboss.modules.ModuleClassLoader cannot be cast to java.net.URLClassLoader")) {
                throw new CompilationException("Error while resolving XSD inclusion", e);
            } else {
                throw e;
            }
        }
    }

}
