package net.ihe.gazelle.daffodil;

import edu.illinois.ncsa.daffodil.japi.Diagnostic;

import java.util.List;

class DiagnosticUtil {

    static String extractDiagnostics(List<Diagnostic> diagnostics) {
        StringBuilder sb = new StringBuilder();
        for (Diagnostic diagnostic : diagnostics) {
            sb.append(diagnostic.getSomeMessage());
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }

}
