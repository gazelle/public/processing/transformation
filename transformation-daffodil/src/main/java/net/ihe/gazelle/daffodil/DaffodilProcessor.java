package net.ihe.gazelle.daffodil;

import edu.illinois.ncsa.daffodil.japi.DataProcessor;
import edu.illinois.ncsa.daffodil.japi.ParseResult;
import edu.illinois.ncsa.daffodil.japi.UnparseResult;
import edu.illinois.ncsa.daffodil.japi.infoset.InfosetOutputter;
import edu.illinois.ncsa.daffodil.japi.infoset.XMLTextInfosetInputter;
import edu.illinois.ncsa.daffodil.japi.infoset.XMLTextInfosetOutputter;
import net.ihe.gazelle.transformation.service.TransformationException;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;

/**
 * Transform data into XML or XML into data according to the loaded DFDL schema.
 * A {@link DaffodilProcessor} must be build using {@link DaffodilProcessorFactory}.
 *
 * @author ceoche
 * @see DaffodilProcessorFactory
 */
public class DaffodilProcessor {

    private DataProcessor dataProcessor;

    DaffodilProcessor(DataProcessor dataProcessor) {
        this.dataProcessor = dataProcessor;
    }

    /**
     * Save the processor into a file (it will create silently all parent-directory if necessary).
     * <p>
     * It is generally faster to reload a processor from a save file rather than recompiling the schema each time.
     * Once the processor has been saved it can be reloaded calling {@link DaffodilProcessorFactory#reloadProcessor(File)}.
     *
     * @param saveFile target file where to save the processor
     * @throws IOException in case of file access error
     */
    public void save(File saveFile) throws IOException {
        saveFile.getParentFile().mkdirs();
        try (FileOutputStream fos = new FileOutputStream(saveFile)) {
            dataProcessor.save(fos.getChannel());
        } catch (FileNotFoundException e) {
            throw new IOException("Unable to save DFDL schema compilation: " + e.getMessage(), e);
        }
    }


    /**
     * Make the transformation of data into XML
     *
     * @param inData is the data to transform. it can be any byte array respecting the DFDL schema
     * @return the data restructered into an XML String (UTF-8 String)
     * @throws TransformationException in case of error while processing the data. Then invoke {@link TransformationException#getMessage()}
     *                                 to get transformation details and eventually {@link TransformationException#getIncompleteTransformation()}
     *                                 can be called to get a partial
     *                                 transformation.
     */
    public String transformDataToXML(byte[] inData) throws TransformationException {

        if (inData != null && inData.length > 0) {

            ByteArrayInputStream baisData = new ByteArrayInputStream(inData);
            ReadableByteChannel rbcData = Channels.newChannel(baisData);

            Writer writer = new StringWriter();
            InfosetOutputter infosetOutputter = new XMLTextInfosetOutputter(writer, false);

            ParseResult parseResult = dataProcessor.parse(rbcData, infosetOutputter);

            String output = writer.toString();
            IOUtils.closeQuietly(rbcData);
            IOUtils.closeQuietly(writer);

            if (!parseResult.isError()) {
                if (parseResult.location().isAtEnd()) {
                    return output;
                } else {
                    throw new TransformationException("Incomplete transformation, data mights contain an error",
                            output.getBytes(StandardCharsets.UTF_8));
                }
            } else {
                throw new TransformationException(DiagnosticUtil.extractDiagnostics(parseResult.getDiagnostics()));
            }

        } else {
            throw new TransformationException("Data to transform is null or empty");
        }

    }


    /**
     * Make the transformation from XML to data
     *
     * @param inXML is the XML String (UTF-8 String) to transform into raw data
     * @return a byte array corresponding to the given XML in compliance with the DFDL Schema
     * @throws TransformationException in case of error while processing the XML. Then invoke {@link TransformationException#getMessage()}
     *                                 to get transformation details and eventually {@link TransformationException#getIncompleteTransformation()}
     *                                 can be called to get a partial
     *                                 transformation.
     */
    public byte[] transformXMLToData(String inXML) throws TransformationException {

        if (inXML != null && !inXML.isEmpty()) {

            Reader reader = new StringReader(inXML);
            XMLTextInfosetInputter infosetInputter = new XMLTextInfosetInputter(reader);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            WritableByteChannel wbc = Channels.newChannel(baos);

            UnparseResult unparseResult = dataProcessor.unparse(infosetInputter, wbc);

            byte[] output = baos.toByteArray();
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(baos);

            if (!unparseResult.isError()) {
                return output;
            } else {
                throw new TransformationException(DiagnosticUtil.extractDiagnostics(unparseResult.getDiagnostics()));
            }

        } else {
            throw new TransformationException("XML to transform is null or empty");
        }
    }


}
