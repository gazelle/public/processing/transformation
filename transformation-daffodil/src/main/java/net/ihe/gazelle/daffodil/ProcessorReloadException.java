package net.ihe.gazelle.daffodil;

/**
 * Exception use to handle Daffodil processor reloading errors
 *
 * @author ceoche
 */
public class ProcessorReloadException extends Exception {

    private static final long serialVersionUID = 3130905165345276390L;

    public ProcessorReloadException(String message) {
        super(message);
    }

    public ProcessorReloadException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
