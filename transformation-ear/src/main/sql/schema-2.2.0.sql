--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255) NOT NULL
);


ALTER TABLE public.app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE public.cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_home_id_seq OWNER TO gazelle;

--
-- Name: trans_dfdl_schema; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.trans_dfdl_schema (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    author character varying(255) NOT NULL,
    available boolean,
    description character varying(255),
    keyword character varying(255),
    name character varying(255) NOT NULL,
    path character varying(255),
    version character varying(255) NOT NULL,
    object_type_id integer
);


ALTER TABLE public.trans_dfdl_schema OWNER TO gazelle;

--
-- Name: trans_dfdl_schema_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.trans_dfdl_schema_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trans_dfdl_schema_id_seq OWNER TO gazelle;

--
-- Name: trans_object_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.trans_object_type (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    keyword character varying(255),
    name character varying(255)
);


ALTER TABLE public.trans_object_type OWNER TO gazelle;

--
-- Name: trans_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.trans_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trans_object_type_id_seq OWNER TO gazelle;

--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: trans_dfdl_schema daffodil_transformation_dfdl_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.trans_dfdl_schema
    ADD CONSTRAINT daffodil_transformation_dfdl_file_pkey PRIMARY KEY (id);


--
-- Name: trans_object_type daffodil_transformation_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.trans_object_type
    ADD CONSTRAINT daffodil_transformation_object_type_pkey PRIMARY KEY (id);


--
-- Name: app_configuration uk_app_configuration_variable; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.app_configuration
    ADD CONSTRAINT uk_app_configuration_variable UNIQUE (variable);


--
-- Name: trans_dfdl_schema uk_lo5cslngngyp0fophmgbvwrdp; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.trans_dfdl_schema
    ADD CONSTRAINT uk_lo5cslngngyp0fophmgbvwrdp UNIQUE (keyword);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: trans_object_type uk_ohkbj3qngl9vkufma34rkn81u; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.trans_object_type
    ADD CONSTRAINT uk_ohkbj3qngl9vkufma34rkn81u UNIQUE (keyword);


--
-- Name: trans_dfdl_schema fk_gp55c8vvytvgmhc5abu73rkdg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.trans_dfdl_schema
    ADD CONSTRAINT fk_gp55c8vvytvgmhc5abu73rkdg FOREIGN KEY (object_type_id) REFERENCES public.trans_object_type(id);


--
-- PostgreSQL database dump complete
--

