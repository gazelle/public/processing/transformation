-- Update Gazelle Transformation from 1.0.9 to 2.0.0 version
-- Author: ceoche

\c postgres
ALTER DATABASE "daffodil-transformation" RENAME TO transformation ;
\c transformation

-- *** Update table app_configuration

-- Delete null and duplicated variables
DELETE FROM app_configuration WHERE variable IS NULL ;
DELETE FROM app_configuration WHERE id IN (SELECT ap1.id FROM app_configuration ap1 INNER JOIN app_configuration ap2 ON ap1.variable = ap2.variable AND ap1.id > ap2.id) ;
-- Add unique and not null constraints
ALTER TABLE ONLY app_configuration ADD CONSTRAINT uk_app_configuration_variable UNIQUE (variable);
ALTER TABLE app_configuration ALTER COLUMN variable SET NOT NULL;

UPDATE app_configuration SET value = 'Gazelle Transformation' WHERE variable = 'application_name' ;
UPDATE app_configuration SET value = '/transformation' WHERE variable = 'application_url_basename' ;
UPDATE app_configuration SET value = REPLACE(value, 'DaffodilTransformationGUI', 'transformation') WHERE variable = 'application_url' ;

-- *** Update table dfdl schema

ALTER TABLE daffodil_transformation_dfdl_file RENAME TO trans_dfdl_schema ;
ALTER SEQUENCE daffodil_transformation_dfdl_file_id_seq RENAME TO trans_dfdl_schema_id_seq ;

ALTER TABLE trans_dfdl_schema RENAME label TO keyword ;

-- *** Update table object type

ALTER TABLE daffodil_transformation_object_type RENAME TO trans_object_type ;
ALTER SEQUENCE daffodil_transformation_object_type_id_seq RENAME TO trans_object_type_id_seq ;

ALTER TABLE trans_object_type RENAME label_to_display to name ;
