INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'true', 'cas_enabled');
DELETE FROM app_configuration WHERE variable = 'application_works_without_cas';
DELETE FROM app_configuration WHERE variable = 'cas_url';
