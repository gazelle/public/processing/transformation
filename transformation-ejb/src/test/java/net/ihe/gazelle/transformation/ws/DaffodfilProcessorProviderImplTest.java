package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.daffodil.DaffodilProcessor;
import net.ihe.gazelle.transformation.dao.DfdlSchemaDAOImpl;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.service.CompilationException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RunWith(MockitoJUnitRunner.class)
public class DaffodfilProcessorProviderImplTest {

    private static final String RESOURCE_FOLDER = "src/test/resources/";

    @Mock
    private DfdlSchema dfdlSchema;
    @Mock
    private DfdlSchemaDAOImpl dfdlSchemaDAO;

    @InjectMocks
    private DaffodilProcessorProviderImpl processorProvider;

    private File schemaFile = new File(RESOURCE_FOLDER + "valid_rsp580.xsd");
    private File compiledFile = new File(RESOURCE_FOLDER + "/compilations/valid_rsp580.xsd.compiled");
    private Path tmpDir;
    private File tmpCompiledFile;


    @Before
    public void setUp() throws IOException {
        tmpDir = Files.createTempDirectory("transformsrv-test");
        tmpCompiledFile = new File(tmpDir + "/compilations/valid_rsp580.xsd.compiled");
    }

    @After
    public void tearDown() {
        tmpCompiledFile.delete();
    }

    @Test
    public void getProcessorFromDfdlReferenceNotCompiledTest() throws IOException, CompilationException {

        Mockito.when(dfdlSchemaDAO.isCompiled(dfdlSchema)).thenReturn(false);
        Mockito.when(dfdlSchemaDAO.getDfdlSchemaFile(dfdlSchema)).thenReturn(schemaFile);
        Mockito.when(dfdlSchemaDAO.getCompiledFile(dfdlSchema)).thenReturn(tmpCompiledFile);

        DaffodilProcessor daffodilProcessor = processorProvider.getProcessorFromDfdlReference(dfdlSchema);
        Assert.assertNotNull("Daffodil processor must no be null", daffodilProcessor);
        Assert.assertTrue("Compiled file must exists (save method must have created intermediary dirs", compiledFile.exists());
    }

    @Test
    public void getProcessorFromDfdlReferenceReloadTest() throws IOException, CompilationException {

        Mockito.when(dfdlSchemaDAO.isCompiled(dfdlSchema)).thenReturn(true);
        Mockito.when(dfdlSchemaDAO.getCompiledFile(dfdlSchema)).thenReturn(compiledFile);

        DaffodilProcessor daffodilProcessor = processorProvider.getProcessorFromDfdlReference(dfdlSchema);
        Assert.assertNotNull("Daffodil processor must no be null", daffodilProcessor);
        Assert.assertTrue("Compiled file must exists (save method must have created intermediary dirs", compiledFile.exists());
    }

    @Test
    public void getProcessorFromGivenDfdlFileTest() throws IOException, CompilationException {

        byte[] dfdlSchema = Files.readAllBytes(schemaFile.toPath());

        DaffodilProcessor daffodilProcessor = processorProvider.getProcessorFromGivenDfdlSchema(dfdlSchema);
        Assert.assertNotNull("Daffodil processor must no be null", daffodilProcessor);

    }

}
