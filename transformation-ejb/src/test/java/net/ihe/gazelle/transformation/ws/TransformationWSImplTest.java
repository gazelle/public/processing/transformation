package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.daffodil.DaffodilProcessor;
import net.ihe.gazelle.daffodil.DaffodilProcessorFactory;
import net.ihe.gazelle.transformation.dao.DfdlSchemaDAO;
import net.ihe.gazelle.transformation.dao.ObjectTypeDAO;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.model.ObjectType;
import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.TransformationException;
import net.ihe.gazelle.transformation.service.TransformedDataWithDFDLReference;
import net.ihe.gazelle.transformation.service.TransformedXmlWithDFDLReference;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author cquere, ceoche
 */
@RunWith(MockitoJUnitRunner.class)
public class TransformationWSImplTest {

    private static final String RESOURCES_DIRECTORY = "src/test/resources/twlab/";
    private static final String VALID_SCHEMA_PATH = RESOURCES_DIRECTORY + "mms_mmxu.dfdl.xsd";
    private static final String INVALID_SCHEMA_PATH = RESOURCES_DIRECTORY + "invalid_mms_mmxu.dfdl.xsd";
    private static final String VALID_DATA_PATH = RESOURCES_DIRECTORY + "payload_157.asn1";
    private static final String VALID_XML_PATH = RESOURCES_DIRECTORY + "payloadTransformed.xml";
    private static final String MMS_MMXU_KEYWORD = "MMS_MMXU";

    @Mock
    private DaffodilProcessorProvider processorProviderMock;
    @Mock
    private DfdlSchemaDAO dfdlSchemaDAOMock;
    @Mock
    private ObjectTypeDAO objectTypeDAOMock;

    @InjectMocks
    private TransformationWSImpl transformationWSImpl;

    DaffodilProcessor localProcessor;
    DfdlSchema dfdlSchema;
    ObjectType objectType;
    private byte[] validDfdl;
    private byte[] invalidDfdl;
    private byte[] data;
    private byte[] xml;

    @Before
    public void setUp() throws IOException, CompilationException {

        objectType = new ObjectType();
        objectType.init();
        objectType.setKeyword("MMS");
        objectType.setName("MMS");
        objectType.setDescription("Short description");

        dfdlSchema = new DfdlSchema();
        dfdlSchema.init();
        dfdlSchema.setKeyword(MMS_MMXU_KEYWORD);
        dfdlSchema.setName("MMS MMXU Schema");
        dfdlSchema.setDescription("Longer description");
        dfdlSchema.setVersion("61440");
        dfdlSchema.setAuthor("ceoche");
        dfdlSchema.setAvailable(true);
        dfdlSchema.setObjectType(objectType);

        validDfdl = getFileContent(VALID_SCHEMA_PATH);
        invalidDfdl = getFileContent(INVALID_SCHEMA_PATH);
        data = getFileContent(VALID_DATA_PATH);
        xml = getFileContent(VALID_XML_PATH);

        localProcessor = DaffodilProcessorFactory.compileProcessor(new File(VALID_SCHEMA_PATH));
    }

    /**
     * Testing transformDataGivenDfdlSchema with a valid DFDL file and a valid data file
     */
    @Test
    public void transformDataGivenDfdlSchemaWSTest() throws Exception {

        Mockito.when(processorProviderMock.getProcessorFromGivenDfdlSchema(validDfdl)).thenReturn(localProcessor);

        byte[] transformedXml = transformationWSImpl.transformDataGivenDfdlSchema(validDfdl, data);

        Assert.assertTrue("transformedXml must have content", transformedXml != null && transformedXml.length > 0);
        Assert.assertArrayEquals("Transformed data must be equals to expected", xml, transformedXml);

    }

    /**
     * Testing transformDataGivenDfdlSchema with an invalid DFDL file and a valid data file
     */
    @Test(expected = CompilationException.class)
    public void transformDataGivenDfdlSchemaWSErrorTest() throws Exception {

        Mockito.when(processorProviderMock.getProcessorFromGivenDfdlSchema(invalidDfdl))
                .thenThrow(new IOException("compilation failure mocked exception"));

        byte[] transformedXml = transformationWSImpl.transformDataGivenDfdlSchema(invalidDfdl, data);

    }

    @Test
    public void transformXMLGivenDfdlSchemaWSTest() throws Exception {

        Mockito.when(processorProviderMock.getProcessorFromGivenDfdlSchema(validDfdl)).thenReturn(localProcessor);

        byte[] transformedData = transformationWSImpl.transformXMLGivenDfdlSchema(validDfdl, xml);

        Assert.assertTrue("transformedData must have content", transformedData != null && transformedData.length > 0);
        Assert.assertArrayEquals("Transformed data must be equals to expected", data, transformedData);

    }

    @Test(expected = CompilationException.class)
    public void transformXMLGivenDfdlSchemaWSErrorTest() throws Exception {

        Mockito.when(processorProviderMock.getProcessorFromGivenDfdlSchema(invalidDfdl))
                .thenThrow(new IOException("compilation failure mocked exception"));

        byte[] transformedData = transformationWSImpl.transformXMLGivenDfdlSchema(invalidDfdl, xml);

    }

    @Test
    public void transformDataGivenDfdlReferenceTest() throws CompilationException, TransformationException, ObjectNotFoundException, IOException {

        Mockito.when(dfdlSchemaDAOMock.getByKeyword(MMS_MMXU_KEYWORD)).thenReturn(dfdlSchema);
        Mockito.when(processorProviderMock.getProcessorFromDfdlReference(dfdlSchema)).thenReturn(localProcessor);

        TransformedXmlWithDFDLReference transformedXmlWithDFDLReference = transformationWSImpl
                .transformDataGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, data);

        Assert.assertTrue("transformedXmlWithDFDLReference must contains transformed XML",
                transformedXmlWithDFDLReference != null && transformedXmlWithDFDLReference
                        .getTransformedXml() != null && transformedXmlWithDFDLReference.getTransformedXml().length > 0);

        Assert.assertArrayEquals("Transformed XML must be equals to expected", xml, transformedXmlWithDFDLReference.getTransformedXml());
        Assert.assertEquals("The referenced DFDL Schema must be MMS MMXU ", MMS_MMXU_KEYWORD,
                transformedXmlWithDFDLReference.getDfdlSchema().getKeyword());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void transformDataGivenDfdlReferenceNoObjectErrorTest() throws CompilationException, TransformationException, ObjectNotFoundException {
        Mockito.when(dfdlSchemaDAOMock.getByKeyword(MMS_MMXU_KEYWORD)).thenReturn(null);

        transformationWSImpl.transformDataGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, data);
    }

    @Test(expected = CompilationException.class)
    public void transformDataGivenDfdlReferenceAccessXSDErrorTest()
            throws CompilationException, TransformationException, ObjectNotFoundException, IOException {

        Mockito.when(dfdlSchemaDAOMock.getByKeyword(MMS_MMXU_KEYWORD)).thenReturn(dfdlSchema);
        Mockito.when(processorProviderMock.getProcessorFromDfdlReference(dfdlSchema))
                .thenThrow(new IOException("compilation failure mocked exception"));

        transformationWSImpl.transformDataGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformDataGivenReferenceNoArgKeywordErrorTest() throws CompilationException, TransformationException, ObjectNotFoundException {
        transformationWSImpl.transformDataGivenReferenceToDfdlSchema(null, data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformDataGivenReferenceNoArgDataErrorTest() throws CompilationException, TransformationException, ObjectNotFoundException {
        transformationWSImpl.transformDataGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, null);
    }

    @Test
    public void transformXMLGivenDfdlReferenceTest() throws CompilationException, TransformationException, ObjectNotFoundException, IOException {

        Mockito.when(dfdlSchemaDAOMock.getByKeyword(MMS_MMXU_KEYWORD)).thenReturn(dfdlSchema);
        Mockito.when(processorProviderMock.getProcessorFromDfdlReference(dfdlSchema)).thenReturn(localProcessor);

        TransformedDataWithDFDLReference transformedDataWithDFDLReference = transformationWSImpl
                .transformXMLGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, xml);

        Assert.assertTrue("transformedXmlWithDFDLReference must contains transformed Data",
                transformedDataWithDFDLReference != null && transformedDataWithDFDLReference
                        .getTransformedData() != null && transformedDataWithDFDLReference.getTransformedData().length > 0);

        Assert.assertArrayEquals("Transformed XML must be equals to expected", data, transformedDataWithDFDLReference.getTransformedData());
        Assert.assertEquals("The referenced DFDL Schema must be MMS MMXU ", MMS_MMXU_KEYWORD,
                transformedDataWithDFDLReference.getDfdlSchema().getKeyword());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void transformXMLGivenDfdlReferenceNoObjectErrorTest() throws CompilationException, TransformationException, ObjectNotFoundException {
        Mockito.when(dfdlSchemaDAOMock.getByKeyword(MMS_MMXU_KEYWORD)).thenReturn(null);

        transformationWSImpl.transformXMLGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, data);
    }

    @Test(expected = CompilationException.class)
    public void transformXMLlGivenDfdlReferenceAccessXSDErrorTest()
            throws CompilationException, TransformationException, ObjectNotFoundException, IOException {

        Mockito.when(dfdlSchemaDAOMock.getByKeyword(MMS_MMXU_KEYWORD)).thenReturn(dfdlSchema);
        Mockito.when(processorProviderMock.getProcessorFromDfdlReference(dfdlSchema))
                .thenThrow(new IOException("compilation failure mocked exception"));

        transformationWSImpl.transformXMLGivenReferenceToDfdlSchema(MMS_MMXU_KEYWORD, data);
    }


    private byte[] getFileContent(String filePath) throws IOException {
        return Files.readAllBytes(Paths.get(filePath));
    }

}
