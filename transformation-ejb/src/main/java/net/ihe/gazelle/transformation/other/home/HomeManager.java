package net.ihe.gazelle.transformation.other.home;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

@Name("homeManagerBean")
@Scope(ScopeType.PAGE)
public class HomeManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1111092307583013570L;

    private Home selectedHome;
    private boolean mainContentEditMode;

    public Home getSelectedHome() {
        return this.selectedHome;
    }

    public void setSelectedHome(Home selectedHome) {
        this.selectedHome = selectedHome;
    }

    public boolean isMainContentEditMode() {
        return this.mainContentEditMode;
    }

    public void setMainContentEditMode(boolean mainContentEditMode) {
        this.mainContentEditMode = mainContentEditMode;
    }

    public void initializeHome() {
        this.selectedHome = Home.getHomeForSelectedLanguage();
    }

    public void editMainContent() {
        this.mainContentEditMode = true;
    }

    public void save() {
        this.selectedHome = this.selectedHome.save();
        this.mainContentEditMode = false;
    }

    public void cancel() {
        this.mainContentEditMode = false;
    }

}
