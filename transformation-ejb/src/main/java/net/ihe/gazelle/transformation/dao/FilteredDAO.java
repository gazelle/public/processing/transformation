package net.ihe.gazelle.transformation.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;

public interface FilteredDAO<T> {

    Filter<T> createFilter();

    FilterDataModel<T> getFiltered(Filter<T> filter);

}
