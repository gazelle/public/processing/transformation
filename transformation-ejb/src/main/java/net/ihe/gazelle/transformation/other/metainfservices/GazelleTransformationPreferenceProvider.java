package net.ihe.gazelle.transformation.other.metainfservices;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.transformation.other.preferences.ApplicationConfiguration;
import net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationQuery;
import org.jboss.seam.contexts.Contexts;
import org.kohsuke.MetaInfServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Access to Gazelle Transformation Application Preferences in a BEAN-TransactionManagementType way. I.e. without having transaction and context
 * handled by the container.
 * Call {@link net.ihe.gazelle.preferences.PreferenceService} to get this provider.
 * <p>
 * If you are in a CONTAINER-TransactionManagementType, you should use instead
 * {@link net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationDAO} or
 * {@link net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationProvider} and injection.
 *
 * @author ceoche
 */
@MetaInfServices(PreferenceProvider.class)
public class GazelleTransformationPreferenceProvider implements PreferenceProvider {

    private static final String DATE_PATTERN = "YYYYMMDDHHmmss";
    private static final Logger LOG = LoggerFactory.getLogger(GazelleTransformationPreferenceProvider.class);

    public GazelleTransformationPreferenceProvider() {
        //Required by MetaInfService
    }

    @Override
    public Boolean getBoolean(String key) {
        String prefAsString = getString(key);
        if (prefAsString != null && !prefAsString.isEmpty()) {
            return Boolean.parseBoolean(prefAsString);
        } else {
            return null;
        }
    }

    @Override
    public Date getDate(String arg0) {
        String prefAsString = getString(arg0);
        if (prefAsString != null) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
                return sdf.parse(prefAsString);
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Integer getInteger(String key) {
        String prefAsString = getString(key);
        if (prefAsString != null && !prefAsString.isEmpty()) {
            try {
                return Integer.parseInt(prefAsString);
            } catch (NumberFormatException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Object getObject(Object arg0) {
        return null;
    }

    @Override
    public String getString(String key) {
        ApplicationConfiguration appConfig = getApplicationConfiguration(key);
        return appConfig != null ? appConfig.getValue() : null;
    }

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return -100;
        } else {
            return 100;
        }
    }

    @Override
    public void setBoolean(String arg0, Boolean arg1) {
        setString(arg0, arg1.toString());
    }

    @Override
    public void setDate(String arg0, Date arg1) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
            setString(arg0, sdf.format(arg1));
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void setInteger(String arg0, Integer arg1) {
        if (arg1 != null) {
            setString(arg0, arg1.toString());
        } else {
            setString(arg0, null);
        }
    }

    @Override
    public void setObject(Object arg0, Object arg1) {

    }

    @Override
    public void setString(String arg0, String arg1) {
        ApplicationConfiguration appConf = getApplicationConfiguration(arg0);
        if (appConf == null) {
            appConf = new ApplicationConfiguration(arg0, arg1);
        } else {
            appConf.setValue(arg1);
        }
        saveApplicationConfiguration(appConf);
    }

    @Override
    public int compareTo(PreferenceProvider o) {
        return getWeight().compareTo(o.getWeight());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof PreferenceProvider) {
            PreferenceProvider pp = (PreferenceProvider) o;
            return this.getWeight().equals(pp.getWeight());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 78;
        int result = super.hashCode();
        result = prime * result + ((this.getWeight() == null) ? 0 : this.getWeight());
        return result;
    }

    private void saveApplicationConfiguration(ApplicationConfiguration appConf) {
        // TODO [ceoche] mutualise cache used in net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationDAOImpl to optimize db
        // access, then reset cache here
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(appConf);
        entityManager.flush();
    }

    private ApplicationConfiguration getApplicationConfiguration(String variable) {
        // TODO [ceoche] mutualise cache used in net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationDAOImpl to optimize db
        // access, then add key-value in cache here
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery();
        query.variable().eq(variable);
        return query.getUniqueResult();
    }
}
