package net.ihe.gazelle.transformation.model;

import net.ihe.gazelle.transformation.service.DFDLSchemaEntry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * Created by cquere on 26/04/17.
 * <p>
 * This class describes the {@link DfdlSchema} object that contain all information about the DFDL file in the database
 */


@Entity
@Table(name = "trans_dfdl_schema", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "trans_dfdl_schema_sequence", sequenceName = "trans_dfdl_schema_id_seq", allocationSize = 1)
public class DfdlSchema extends Audited implements Serializable {

    private static final long serialVersionUID = -6619466590020344345L;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "trans_dfdl_schema_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "keyword", unique = true, nullable = false)
    private String keyword;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "version", nullable = false)
    private String version;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "path")
    private String path;

    @Column(name = "available")
    private boolean available;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "object_type_id")
    private ObjectType objectType;

    public DfdlSchema() {
    }

    public DfdlSchema(String keyword, DfdlSchema dfdlSchema) {
        if (dfdlSchema != null) {
            this.keyword = keyword;
            this.name = dfdlSchema.getName();
            this.version = dfdlSchema.getVersion();
            this.author = dfdlSchema.getAuthor();
            this.path = dfdlSchema.getPath();
            this.available = dfdlSchema.isAvailable();
            this.description = dfdlSchema.getDescription();
            this.objectType = dfdlSchema.getObjectType();
        }
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String label) {
        this.keyword = label;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isAvailable() {
        return this.available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public ObjectType getObjectType() {
        return this.objectType;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    public DFDLSchemaEntry getAsDFDLSchemaEntry() {
        return new DFDLSchemaEntry(keyword, name, version, lastChanged, description, objectType.getAsObjectTypeEntry());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.available ? 1231 : 1237);
        result = prime * result + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + ((this.keyword == null) ? 0 : this.keyword.hashCode());
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        result = prime * result + ((this.path == null) ? 0 : this.path.hashCode());
        result = prime * result + ((this.version == null) ? 0 : this.version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        DfdlSchema other = (DfdlSchema) obj;
        if (this.available != other.available) {
            return false;
        }
        if (this.description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!this.keyword.equals(other.keyword)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        if (this.path == null) {
            if (other.path != null) {
                return false;
            }
        } else if (!this.path.equals(other.path)) {
            return false;
        }
        if (this.version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!this.version.equals(other.version)) {
            return false;
        }
        return true;
    }

}
