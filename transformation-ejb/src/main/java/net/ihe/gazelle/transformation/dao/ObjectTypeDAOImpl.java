package net.ihe.gazelle.transformation.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.transformation.model.DfdlSchemaQuery;
import net.ihe.gazelle.transformation.model.ObjectType;
import net.ihe.gazelle.transformation.model.ObjectTypeQuery;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Stateless
@AutoCreate
@Name("objectTypeDAO")
public class ObjectTypeDAOImpl implements ObjectTypeDAO {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public ObjectType getById(Integer id) {
        if (id == null || id <= 0) {
            return null;
        }
        return entityManager.find(ObjectType.class, id);
    }

    @Override
    public ObjectType getByKeyword(String inKeyword) {
        if (inKeyword == null || inKeyword.length() == 0) {
            return null;
        }
        ObjectTypeQuery otq = new ObjectTypeQuery(entityManager);
        otq.keyword().eq(inKeyword);
        return otq.getUniqueResult();
    }

    @Override
    public List<ObjectType> getAll() {
        Query query = entityManager.createQuery("select ot from ObjectType ot order by ot.name");
        return (List<ObjectType>) query.getResultList();
    }

    @Override
    public ObjectType save(ObjectType objectType) throws EntityConstraintException {
        if (objectType != null) {
            try {
                objectType = entityManager.merge(objectType);
                entityManager.flush();
            } catch (PersistenceException e) {
                if (e.getCause() instanceof ConstraintViolationException) {
                    throw new EntityConstraintException((ConstraintViolationException) e.getCause());
                } else {
                    throw e;
                }
            }
        }
        return objectType;
    }

    @Override
    public void delete(ObjectType objectType) {
        if (objectType != null) {
            objectType = entityManager.find(ObjectType.class, objectType.getId());
            entityManager.remove(objectType);
            entityManager.flush();
        }
    }

    @Override
    public boolean isKeywordAlreadyUsed(String keyword) {
        ObjectTypeQuery objectTypeQuery = new ObjectTypeQuery(entityManager);
        objectTypeQuery.keyword().eq(keyword);
        return objectTypeQuery.getCount() != 0;
    }

    @Override
    public boolean isObjectTypeReferenced(ObjectType objectType) {
        DfdlSchemaQuery schemaQuery = new DfdlSchemaQuery(entityManager);
        schemaQuery.objectType().id().eq(objectType.getId());
        return schemaQuery.getCount() > 0;
    }

    @Override
    public Filter<ObjectType> createFilter() {
        return new Filter<ObjectType>(getHQLCriteriaForFilter());
    }

    @Override
    public FilterDataModel<ObjectType> getFiltered(Filter<ObjectType> filter) {
        return new FilterDataModel<ObjectType>(filter) {
            @Override
            protected Object getId(ObjectType objectType) {
                return objectType.getId();
            }
        };
    }

    protected HQLCriterionsForFilter<ObjectType> getHQLCriteriaForFilter() {
        ObjectTypeQuery query = new ObjectTypeQuery(entityManager);
        HQLCriterionsForFilter<ObjectType> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("keyword", query.keyword());
        criteria.addPath("name", query.name());

        criteria.addQueryModifier(new QueryModifier<ObjectType>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<ObjectType> hqlQueryBuilder, Map<String, Object> map) {

            }
        });

        return criteria;
    }
}
