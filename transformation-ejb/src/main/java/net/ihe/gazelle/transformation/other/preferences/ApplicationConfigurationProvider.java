package net.ihe.gazelle.transformation.other.preferences;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * This seam bean is used to quickly access ApplicationConfiguration preferences from JSF pages or other seam bean.
 * Values will be typed according to their meaning.
 * <p>
 * The cache is handle by the underlying EJB {@link ApplicationConfigurationDAOImpl}
 *
 * @author aberge, ceoche
 */

@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("applicationConfigurationProvider")
@GenerateInterface("ApplicationConfigurationProviderLocal")
public class ApplicationConfigurationProvider implements Serializable, ApplicationConfigurationProviderLocal {

    private static final long serialVersionUID = 5004043953931007769L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfigurationProvider.class);

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    /**
     * Get the singleton seam bean applicationConfigurationProvider
     *
     * @return this bean
     * @deprecated [ceoche] This method create a hard dependency to this implementation. You should use injection over the interface instead.
     */
    @Deprecated
    public static ApplicationConfigurationProviderLocal instance() {
        return (ApplicationConfigurationProviderLocal) Component.getInstance("applicationConfigurationProvider");
    }

    @Override
    public boolean getApplicationWorksWithoutCas() {
        return Boolean.parseBoolean(applicationConfigurationDAO.getValue(Keys.APPLICATION_WORKS_WITHOUT_CAS.getVariable()));
    }

    @Override
    public boolean getIpLogin() {
        return Boolean.parseBoolean(applicationConfigurationDAO.getValue(Keys.IP_LOGIN.getVariable()));
    }

    @Override
    public String getIpLoginAdmin() {
        return applicationConfigurationDAO.getValue(Keys.IP_LOGIN_ADMIN.getVariable());
    }

    @Override
    public String getApplicationUrl() {
        return applicationConfigurationDAO.getValue(Keys.APPLICATION_URL.getVariable());
    }

    // TODO Add methods for getting other variable values.

    public enum Keys {

        APPLICATION_ADMIN_EMAIL("application_admin_email"),
        APPLICATION_ADMIN_NAME("application_admin_name"),
        APPLICATION_ADMIN_TITLE("application_admin_title"),
        APPLICATION_NAME("application_name"),
        APPLICATION_RELEASE_NOTES_URL("application_release_notes_url"),
        APPLICATION_URL("application_url"),
        APPLICATION_URL_BASENAME("application_url_basename"),
        APPLICATION_WORKS_WITHOUT_CAS("application_works_without_cas"),
        BIN_PATH("bin_path"),
        DOCUMENTATION_URL("documentation_url"),
        IP_LOGIN("ip_login"),
        IP_LOGIN_ADMIN("ip_login_admin"),
        NUMBER_OF_ITEMS_PER_PAGE("NUMBER_OF_ITEMS_PER_PAGE"),
        TIME_ZONE("time_zone");

        private final String variable;

        Keys(String variable) {
            this.variable = variable;
        }

        public String getVariable() {
            return variable;
        }
    }
}
