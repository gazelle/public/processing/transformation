package net.ihe.gazelle.transformation.other.preferences;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

@Name("applicationConfigurationManager")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 60000)
public class ApplicationConfigurationManager implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfigurationManager.class);

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;
    private ApplicationConfiguration preference = null;

    public List<ApplicationConfiguration> getAllPreferences() {
        return applicationConfigurationDAO.getAll();
    }

    public void savePreference(ApplicationConfiguration inPreference) {
        try {
            applicationConfigurationDAO.save(inPreference);
        } catch (EntityConstraintException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot save preference: {0}", e.getMessage());
        }
    }

    public void createNewPreference() {
        setPreference(new ApplicationConfiguration());
    }

    public ApplicationConfiguration getPreference() {
        return this.preference;
    }

    public void setPreference(ApplicationConfiguration preference) {
        this.preference = preference;
    }

    public void resetPreferenceCache() {
        applicationConfigurationDAO.resetCache();
    }

    public void resetHttpHeaders() {
        LOGGER.info("Reset http headers to default values");

        for (PreferencesKey preferencesKey : PreferencesKey.values()) {

            ApplicationConfiguration securityApplicationConfiguration = applicationConfigurationDAO.getByVariable(preferencesKey.getFriendlyName());
            if (securityApplicationConfiguration == null) {
                securityApplicationConfiguration = new ApplicationConfiguration();
                securityApplicationConfiguration.setVariable(preferencesKey.getFriendlyName());
            }
            securityApplicationConfiguration.setValue(preferencesKey.getDefaultValue());
            try {
                applicationConfigurationDAO.save(securityApplicationConfiguration);
            } catch (EntityConstraintException e) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR, "Cannot save preference '{0}': {1}", securityApplicationConfiguration.getVariable(),
                                e.getMessage());
            }
        }
        updateHttpHeaders();
    }

    public void updateHttpHeaders() {
        CSPHeaderFilter.clearCache();
    }

}
