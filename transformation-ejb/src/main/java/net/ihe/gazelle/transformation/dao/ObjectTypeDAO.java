package net.ihe.gazelle.transformation.dao;

import net.ihe.gazelle.transformation.model.ObjectType;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ObjectTypeDAO extends FilteredDAO<ObjectType> {

    ObjectType getById(Integer id);

    ObjectType getByKeyword(String inKeyword);

    List<ObjectType> getAll();

    ObjectType save(ObjectType selectedObjectType) throws EntityConstraintException;

    void delete(ObjectType objectType);

    boolean isKeywordAlreadyUsed(String keyword);

    boolean isObjectTypeReferenced(ObjectType objectType);
}
