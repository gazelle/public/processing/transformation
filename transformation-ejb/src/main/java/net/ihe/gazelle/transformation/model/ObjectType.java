package net.ihe.gazelle.transformation.model;

import net.ihe.gazelle.transformation.service.ObjectTypeEntry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * Created by cquere on 26/04/17.
 */
@Entity
@Table(name = "trans_object_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "trans_object_type_sequence", sequenceName = "trans_object_type_id_seq", allocationSize = 1)
public class ObjectType extends Audited implements Serializable {

    private static final long serialVersionUID = 2013153390759620929L;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "trans_object_type_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "keyword", nullable = false, unique = true)
    private String keyword;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public ObjectType() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ObjectTypeEntry getAsObjectTypeEntry() {
        return new ObjectTypeEntry(keyword, name, lastChanged, description);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + ((this.keyword == null) ? 0 : this.keyword.hashCode());
        result = prime * result
                + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ObjectType other = (ObjectType) obj;
        if (this.description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!this.keyword.equals(other.keyword)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }
}