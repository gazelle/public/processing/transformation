package net.ihe.gazelle.transformation.other.home;

import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


@Entity
@Name("home")
@Table(name = "cmn_home", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "iso3_language"))
@SequenceGenerator(name = "cmn_home_sequence", sequenceName = "cmn_home_id_seq", allocationSize = 1)
public class Home implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1218389994561421605L;

    @Id
    @GeneratedValue(generator = "cmn_home_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "iso3_language")
    private String iso3Language;

    @Column(name = "main_content")
    @Lob
    @Type(type = "text")
    private String mainContent;

    @Column(name = "home_title")
    private String homeTitle;

    public Home() {

    }

    public Home(String iso3Language) {
        this.iso3Language = iso3Language;
    }

    /**
     * @return : Home
     */
    public static Home getHomeForSelectedLanguage() {
        String language = org.jboss.seam.core.Locale.instance().getISO3Language();
        if ((language != null) && !language.isEmpty()) {
            HomeQuery query = new HomeQuery();
            query.iso3Language().eq(language);
            List<Home> homes = query.getList();
            if ((homes != null) && !homes.isEmpty()) {
                return homes.get(0);
            } else {
                return new Home(language);
            }
        } else {
            return null;
        }
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIso3Language() {
        return this.iso3Language;
    }

    public void setIso3Language(String iso3Language) {
        this.iso3Language = iso3Language;
    }

    public String getMainContent() {
        return this.mainContent;
    }

    public void setMainContent(String mainContent) {
        this.mainContent = mainContent;
    }

    public String getHomeTitle() {
        return this.homeTitle;
    }

    public void setHomeTitle(String homeTitle) {
        this.homeTitle = homeTitle;
    }

    /**
     * @return : Home
     */
    public Home save() {
        EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        Home home = entityManager.merge(this);
        entityManager.flush();
        return home;
    }
}