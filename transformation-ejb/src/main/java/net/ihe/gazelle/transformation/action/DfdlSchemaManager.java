package net.ihe.gazelle.transformation.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.daffodil.DaffodilProcessor;
import net.ihe.gazelle.daffodil.DaffodilProcessorFactory;
import net.ihe.gazelle.transformation.dao.DfdlSchemaDAO;
import net.ihe.gazelle.transformation.dao.ObjectTypeDAO;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.model.ObjectType;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;
import net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationProvider;
import net.ihe.gazelle.transformation.service.CompilationException;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Name("dfdlSchemaManager")
@Scope(ScopeType.PAGE)
public class DfdlSchemaManager implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(DfdlSchemaManager.class);

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;
    @In
    private DfdlSchemaDAO dfdlSchemaDAO;
    @In
    private ObjectTypeDAO objectTypeDAO;
    private Filter<DfdlSchema> filter;
    private DfdlSchema selectedDfdlSchema;

    public DfdlSchema getSelectedDfdlSchema() {
        return this.selectedDfdlSchema;
    }

    public void setSelectedDfdlSchema(DfdlSchema selectedDfdlSchema) {
        this.selectedDfdlSchema = selectedDfdlSchema;
    }

    public void initNewDfdlSchema() {
        this.selectedDfdlSchema = new DfdlSchema();
    }

    public Filter<DfdlSchema> getFilter() {
        if (this.filter == null) {
            this.filter = dfdlSchemaDAO.createFilter();
        }
        return this.filter;
    }

    public FilterDataModel<DfdlSchema> getAllDfdlFiles() {
        return dfdlSchemaDAO.getFiltered(getFilter());
    }

    public void getDfdlFileFromUrl() {
        // If there's an id in the URL, get it and load the selectedDfdlSchema
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.containsKey("id")) {
            Integer dfdlSchemaId = Integer.parseInt(urlParams.get("id"));
            this.selectedDfdlSchema = dfdlSchemaDAO.getById(dfdlSchemaId);
        }
    }

    public void deleteSelectedDfdlFile() {
        String keyword = selectedDfdlSchema.getKeyword();
        dfdlSchemaDAO.delete(selectedDfdlSchema);
        setSelectedDfdlSchema(null);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Dfdl schema " + keyword + " deleted");
    }

    public String displayDfdlFile(DfdlSchema dfdlSchema) {
        return "/schema/view.seam?id=" + dfdlSchema.getId();
    }

    public String editDfdlFile(DfdlSchema dfdlSchema) {
        return "/schema/edit.seam?id=" + dfdlSchema.getId();
    }

    public List<ObjectType> getAllObjectTypes() {
        return objectTypeDAO.getAll();
    }

    public String saveDfdlSchema() {
        try {
            selectedDfdlSchema = dfdlSchemaDAO.save(selectedDfdlSchema);

            File schemaFile = dfdlSchemaDAO.getDfdlSchemaFile(selectedDfdlSchema);
            // Add message to inform user about schema file issue but do not prevent saving
            if (schemaFile == null) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.WARN, "Daffodil XSD schema file not defined for {0}", selectedDfdlSchema.getKeyword());
            } else if (!schemaFile.exists()) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "Daffodil schema '{0}' is missing", schemaFile.getAbsolutePath());
            }

            FacesMessages.instance().add(StatusMessage.Severity.INFO, "Daffodil schema " + this.selectedDfdlSchema.getKeyword() + " saved");
            return "/schema/list.seam";
        } catch (EntityConstraintException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to save daffodil schema: {0}", e.getMessage());
            return null;
        }
    }

    public void copyDfdlFile(DfdlSchema dfdlSchema) {
        if (dfdlSchema != null) {
            String copyKeyword = getCopyKeyword(dfdlSchema.getKeyword());
            DfdlSchema copyDfdlSchema = new DfdlSchema(copyKeyword, dfdlSchema);
            try {
                copyDfdlSchema = dfdlSchemaDAO.save(copyDfdlSchema);
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Daffodil schema " + copyDfdlSchema.getKeyword() + " saved");
            } catch (EntityConstraintException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to save daffodil schema: {0}", e.getMessage());
            }
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "gazelle.daffodiltransformation.no.DfdlFile.selected");
        }
    }

    public String getDfdlFilePathRoot() {
        return applicationConfigurationDAO.getValue(ApplicationConfigurationProvider.Keys.BIN_PATH.getVariable());
    }

    public void compileAvailableDfdlSchemas() {
        List<DfdlSchema> dfdlSchemas = dfdlSchemaDAO.getAvailable();
        List<String> successSchemas = new ArrayList<>();
        List<String> warnSchemas = new ArrayList<>();
        List<String> errorSchemas = new ArrayList<>();

        for (DfdlSchema dfdlSchema : dfdlSchemas) {
            File dfdlSchemaFile = dfdlSchemaDAO.getDfdlSchemaFile(dfdlSchema);
            if (dfdlSchemaFile != null) {
                try {
                    DaffodilProcessor processor = DaffodilProcessorFactory.compileProcessor(dfdlSchemaFile);

                    try {
                        processor.save(dfdlSchemaDAO.getCompiledFile(dfdlSchema));
                        successSchemas.add(dfdlSchema.getKeyword() + " has been compiled and saved");
                    } catch (IOException e) {
                        warnSchemas.add(dfdlSchema.getKeyword() + " has been compiled but not saved: " + e.getMessage());
                    }
                } catch (IOException e) {
                    errorSchemas.add("Fail to compile schema '" + dfdlSchema.getKeyword() +
                            "': Unexpected error when accessing to the DFDL schema file '" + dfdlSchemaFile.getAbsolutePath() +
                            "': " + e.getMessage());
                } catch (CompilationException e) {
                    errorSchemas.add("Fail to compile schema '" + dfdlSchema.getKeyword() + "': " + e.getMessage());
                }
            } else {
                warnSchemas.add(dfdlSchema.getKeyword() + " has been skipped (no XSD Schema defined)");
            }
        }

        if (!successSchemas.isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.INFO, concatStrings(successSchemas));
        }
        if (!warnSchemas.isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.WARN, concatStrings(warnSchemas));
        }
        if (!errorSchemas.isEmpty()) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, concatStrings(errorSchemas));
        }

    }

    public void compileDfdlSchema(DfdlSchema dfdlSchema) {
        File dfdlSchemaFile = dfdlSchemaDAO.getDfdlSchemaFile(dfdlSchema);
        if (dfdlSchemaFile != null) {
            try {
                DaffodilProcessor processor = DaffodilProcessorFactory.compileProcessor(dfdlSchemaFile);
                FacesMessages.instance()
                        .add(StatusMessage.Severity.INFO,
                                "The DFDL schema '" + dfdlSchema.getKeyword() + "' has been compiled with success");

                File dfdlCompiledFile = dfdlSchemaDAO.getCompiledFile(dfdlSchema);
                try {
                    processor.save(dfdlCompiledFile);
                    FacesMessages.instance()
                            .add(StatusMessage.Severity.INFO, "The compilation is available on " + dfdlCompiledFile.getAbsolutePath());
                } catch (IOException e) {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN,
                            "Error while archiving the compilation on " + dfdlCompiledFile.getAbsolutePath() + ": " + e.getMessage());
                }
            } catch (IOException e) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR,
                                "Unexpected error when accessing to the DFDL schema file '" + dfdlSchemaFile.getAbsolutePath() + "': " + e
                                        .getMessage());
            } catch (CompilationException e) {
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR,
                                "An error occured when compiling the DFDL schema '" + dfdlSchema.getKeyword() + "': " + e.getMessage());
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "No DFDL XSD schema defined for {}", dfdlSchema.getKeyword());
        }
    }

    public void updateDfdlFilesVersion() {
        // FIXME high complexity method
        // FIXME Extract generic SVN version retrieving method
        List<DfdlSchema> dfdlSchemas = dfdlSchemaDAO.getAll();
        if (dfdlSchemas != null && !dfdlSchemas.isEmpty()) {
            for (DfdlSchema dfdlSchema : dfdlSchemas) {
                File dfdlSchemaFile = dfdlSchemaDAO.getDfdlSchemaFile(dfdlSchema);
                if (dfdlSchemaFile != null) {
                    if (dfdlSchemaFile.exists()) {
                        Date fileDate = new Date(dfdlSchemaFile.lastModified());
                        if (dfdlSchema.getlastChanged() != null) {
                            if (fileDate.after(dfdlSchema.getlastChanged())) {
                                FileReader fr = null;
                                BufferedReader buff = null;
                                try {
                                    fr = new FileReader(dfdlSchemaFile);
                                    StringBuffer content = new StringBuffer();
                                    buff = new BufferedReader(fr);
                                    String line;
                                    while ((line = buff.readLine()) != null) {
                                        content.append(line);
                                        content.append("\n");
                                    }
                                    buff.close();
                                    String revisionTag = "Revision";
                                    Integer begin = content.indexOf(revisionTag);
                                    if (begin < 0) {
                                        FacesMessages.instance().add("No Revision specified for DfdlSchema " + dfdlSchema.getKeyword());
                                        continue;
                                    } else {
                                        begin = begin + revisionTag.length();
                                        Integer end = content.indexOf("-->", begin);
                                        String newVersion = content.substring(begin, end).replace("=", "").replace(" ", "");
                                        if (newVersion != null && !newVersion.isEmpty()) {
                                            dfdlSchema.setVersion(newVersion);
                                            dfdlSchema = dfdlSchemaDAO.save(dfdlSchema);
                                            if (dfdlSchema != null) {
                                                FacesMessages.instance()
                                                        .add("Version of DfdlSchema " + dfdlSchema
                                                                .getKeyword() + " has been updated to " + newVersion);
                                            }
                                        }
                                    }
                                } catch (IOException | EntityConstraintException e) {
                                    LOGGER.error(e.getMessage());
                                    FacesMessages.instance().add("Version update for " + dfdlSchema.getKeyword() + " failed");
                                } finally {
                                    IOUtils.closeQuietly(fr);
                                    IOUtils.closeQuietly(buff);
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "This file does not exist : " + dfdlSchemaFile.getAbsolutePath());
                        continue;
                    }
                } else {
                    continue;
                }
            }
        } else {
            FacesMessages.instance().addFromResourceBundle("gazelle.daffodiltransformation.no.DfdlFile");
        }
    }

    private String getCopyKeyword(String originalKeyword) {
        String newKeyword = originalKeyword.concat("_COPY");
        if (dfdlSchemaDAO.isKeywordAlreadyUsed(newKeyword)) {
            return getCopyKeyword(newKeyword);
        } else {
            return newKeyword;
        }
    }

    private String concatStrings(List<String> strings) {
        StringBuilder sb = new StringBuilder();
        for (String oneString : strings) {
            sb.append(oneString);
            sb.append(" ; ");
        }
        return sb.toString();
    }

}
