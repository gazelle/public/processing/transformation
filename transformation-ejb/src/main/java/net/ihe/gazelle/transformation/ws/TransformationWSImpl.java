package net.ihe.gazelle.transformation.ws;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.daffodil.DaffodilProcessor;
import net.ihe.gazelle.transformation.dao.DfdlSchemaDAO;
import net.ihe.gazelle.transformation.dao.ObjectTypeDAO;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.model.ObjectType;
import net.ihe.gazelle.transformation.service.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jws.WebService;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * @author ceoche
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@Name("transformationWS")
@WebService(name = "Transformation", targetNamespace = "http://ws.transformation.gazelle.ihe.net/", serviceName = "GazelleTransformationService")
public class TransformationWSImpl implements TransformationWS {

    private static final FhirContext context = FhirContext.forR4();
    private static final Logger LOG = LoggerFactory.getLogger(TransformationWSImpl.class);
    @In(value = "daffodilProcessorProvider")
    private DaffodilProcessorProvider processorProvider;
    @In
    private DfdlSchemaDAO dfdlSchemaDAO;
    @In
    private ObjectTypeDAO objectTypeDAO;

    public byte[] transformXMLGivenDfdlSchema(byte[] dfdlSchema, byte[] xmlToTransform)
            throws CompilationException, TransformationException, IllegalArgumentException {
        checkParamNotNullOrEmpty(dfdlSchema, "dfdlSchema is mandatory");
        checkParamNotNullOrEmpty(xmlToTransform, "xmlToTransform is mandatory");

        DaffodilProcessor processor;
        try {
            processor = processorProvider.getProcessorFromGivenDfdlSchema(dfdlSchema);
        } catch (IOException e) {
            throw new CompilationException("Error while compiling the schema: " + e.getMessage(), e);
        }

        return processor.transformXMLToData(new String(xmlToTransform, StandardCharsets.UTF_8));

    }

    public TransformedDataWithDFDLReference transformXMLGivenReferenceToDfdlSchema(String dfdlSchemaKeyword, byte[] xmlToTransform)
            throws CompilationException, TransformationException, IllegalArgumentException, ObjectNotFoundException {

        checkParamNotNullOrEmpty(dfdlSchemaKeyword, "dfdlSchemaKeyword is mandatory");
        checkParamNotNullOrEmpty(xmlToTransform, "xmlToTransform is mandatory");

        DfdlSchema dfdlSchema = dfdlSchemaDAO.getByKeyword(dfdlSchemaKeyword);
        if (dfdlSchema != null) {

            DaffodilProcessor processor;
            try {
                processor = processorProvider.getProcessorFromDfdlReference(dfdlSchema);
            } catch (IOException e) {
                throw new CompilationException("Error while loading the schema: " + e.getMessage(), e);
            }
            byte[] transformedData = processor.transformXMLToData(new String(xmlToTransform, StandardCharsets.UTF_8));
            return new TransformedDataWithDFDLReference(transformedData, dfdlSchema.getAsDFDLSchemaEntry());

        } else {
            throw new ObjectNotFoundException("Unable to find DFDL schema for keyword '" + dfdlSchemaKeyword + "'");
        }

    }

    public byte[] transformDataGivenDfdlSchema(byte[] dfdlSchema, byte[] dataToTransform)
            throws CompilationException, TransformationException, IllegalArgumentException {

        checkParamNotNullOrEmpty(dfdlSchema, "dfdlSchema is mandatory");
        checkParamNotNullOrEmpty(dataToTransform, "dataToTransform is mandatory");

        DaffodilProcessor processor = null;
        try {
            processor = processorProvider.getProcessorFromGivenDfdlSchema(dfdlSchema);
        } catch (IOException e) {
            throw new CompilationException("Error while compiling the schema: " + e.getMessage(), e);
        }

        return processor.transformDataToXML(dataToTransform).getBytes(StandardCharsets.UTF_8);

    }

    public TransformedXmlWithDFDLReference transformDataGivenReferenceToDfdlSchema(String dfdlSchemaKeyword, byte[] dataToTransform)
            throws CompilationException, TransformationException, IllegalArgumentException, ObjectNotFoundException {

        checkParamNotNullOrEmpty(dfdlSchemaKeyword, "dfdlSchemaKeyword is mandatory");
        checkParamNotNullOrEmpty(dataToTransform, "dataToTransform is mandatory");

        DfdlSchema dfdlSchema = dfdlSchemaDAO.getByKeyword(dfdlSchemaKeyword);
        if (dfdlSchema != null) {
            DaffodilProcessor processor = null;
            try {
                processor = processorProvider.getProcessorFromDfdlReference(dfdlSchema);
            } catch (IOException e) {
                throw new CompilationException("Error while loading the schema: " + e.getMessage(), e);
            }
            byte[] transformedXml = processor.transformDataToXML(dataToTransform).getBytes(StandardCharsets.UTF_8);
            return new TransformedXmlWithDFDLReference(transformedXml, dfdlSchema.getAsDFDLSchemaEntry());
        } else if (dfdlSchemaKeyword.equals("fhirJsonToXML")) {
            try {
                String content = new String(dataToTransform, StandardCharsets.UTF_8);
                byte[] transformedXml = convertJsonToXmlFhir(content);
                return new TransformedXmlWithDFDLReference(transformedXml, null);
            } catch (DataFormatException e) {
                throw new TransformationException("The file provided is not a json file");
            }
        } else if (dfdlSchemaKeyword.equals("JsonToXML")) {
            try {
                String content = new String(dataToTransform, StandardCharsets.UTF_8);
                ObjectMapper objectMapper = new ObjectMapper();
                LinkedHashMap<String, Object> linkedHashMap = objectMapper.readValue(content, new TypeReference<LinkedHashMap<String, Object>>() {});
                StringBuilder xmlBuilder = new StringBuilder();
                buildXmlFromMap(linkedHashMap, xmlBuilder);
                String xml = xmlBuilder.toString();

                return new TransformedXmlWithDFDLReference(xml.getBytes(), null);
            } catch (Exception e) {
                throw new TransformationException("Cannot convert json to xml: check your json file");
            }

        } else {
            throw new ObjectNotFoundException("Unable to find DFDL schema for keyword '" + dfdlSchemaKeyword + "'");
        }
    }

    public static void buildXmlFromMap(Map<String, Object> map, StringBuilder xmlBuilder) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof Map) {
                xmlBuilder.append("<").append(key).append(">");
                buildXmlFromMap((Map<String, Object>) value, xmlBuilder);
                xmlBuilder.append("</").append(key).append(">");
            } else if (value instanceof List) {
                for (Object item : (List<?>) value) {
                    if (item instanceof Map) {
                        xmlBuilder.append("<").append(key).append(">");
                        buildXmlFromMap((Map<String, Object>) item, xmlBuilder);
                        xmlBuilder.append("</").append(key).append(">");
                    } else {
                        xmlBuilder.append("<").append(key).append(">").append(item).append("</").append(key).append(">");
                    }
                }
            } else {
                xmlBuilder.append("<").append(key).append(">").append(value).append("</").append(key).append(">");
            }
        }
    }



    public byte[] convertJsonToXmlFhir(final String content) throws DataFormatException {
        IParser source = context.newJsonParser();                         // new JSON parser
        IBaseResource resource = source.parseResource(content);           // parse the resource
        IParser target = context.newXmlParser();                    // new XML parser
        return target.setPrettyPrint(false).encodeResourceToString(resource).getBytes(StandardCharsets.UTF_8);
    }

    public List<DFDLSchemaEntry> getAvailableDfdlSchemas() throws ObjectNotFoundException {
        List<DFDLSchemaEntry> dfdlSchemaEntries = new ArrayList<>();
        for (DfdlSchema dfdlSchema : dfdlSchemaDAO.getAvailable()) {
            dfdlSchemaEntries.add(dfdlSchema.getAsDFDLSchemaEntry());
        }
        if (!dfdlSchemaEntries.isEmpty()) {
            return dfdlSchemaEntries;
        } else {
            throw new ObjectNotFoundException("No dfdl schema defined");
        }
    }

    public List<ObjectTypeEntry> getAvailableObjectTypes() throws ObjectNotFoundException {
        List<ObjectTypeEntry> objectTypeEntries = new ArrayList<>();
        for (ObjectType objectType : objectTypeDAO.getAll()) {
            objectTypeEntries.add(objectType.getAsObjectTypeEntry());
        }
        if (!objectTypeEntries.isEmpty()) {
            return objectTypeEntries;
        } else {
            throw new ObjectNotFoundException("No object type defined");
        }
    }

    public List<DFDLSchemaEntry> getDfdlSchemasForAGivenObjectType(String objectTypeKeyword)
            throws ObjectNotFoundException, IllegalArgumentException {

        checkParamNotNullOrEmpty(objectTypeKeyword, "objectTypeKeyword is mandatory");

        List<DFDLSchemaEntry> dfdlSchemaEntries = new ArrayList<>();
        for (DfdlSchema dfdlSchema : dfdlSchemaDAO.getAvailableForAGivenObjectType(objectTypeKeyword)) {
            dfdlSchemaEntries.add(dfdlSchema.getAsDFDLSchemaEntry());
        }
        if (!dfdlSchemaEntries.isEmpty()) {
            return dfdlSchemaEntries;
        } else {
            throw new ObjectNotFoundException("No dfdl schema defined for the object type " + objectTypeKeyword);
        }

    }

    public byte[] getDfdlSchemaContentByKeyword(String dfdlSchemaKeyword) throws ObjectNotFoundException, IOException, IllegalArgumentException {

        checkParamNotNullOrEmpty(dfdlSchemaKeyword, "dfdlSchemaKeyword is mandatory");

        DfdlSchema dfdlSchema = dfdlSchemaDAO.getByKeyword(dfdlSchemaKeyword);
        if (dfdlSchema != null) {
            try {
                return Files.readAllBytes(dfdlSchemaDAO.getDfdlSchemaFile(dfdlSchema).toPath());
            } catch (IOException e) {
                throw new IOException("Internal error: unable to read dfdl schema file. " + e.getMessage(), e);
            }
        } else {
            throw new ObjectNotFoundException("No dfdl schema found with label '" + dfdlSchemaKeyword + "'");
        }

    }

    private void checkParamNotNullOrEmpty(String param, String errorMessage) throws IllegalArgumentException {
        if (param == null || param.isEmpty()) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private void checkParamNotNullOrEmpty(byte[] param, String errorMessage) throws IllegalArgumentException {
        if (param == null || param.length == 0) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

}