package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.daffodil.DaffodilProcessor;
import net.ihe.gazelle.daffodil.DaffodilProcessorFactory;
import net.ihe.gazelle.daffodil.ProcessorReloadException;
import net.ihe.gazelle.transformation.dao.DfdlSchemaDAO;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.service.CompilationException;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * {@inheritDoc}
 */
@AutoCreate
@Name("daffodilProcessorProvider")
public class DaffodilProcessorProviderImpl implements DaffodilProcessorProvider {

    private static final Logger LOG = LoggerFactory.getLogger(DaffodilProcessorProviderImpl.class);

    @In
    private DfdlSchemaDAO dfdlSchemaDAO;

    /**
     * {@inheritDoc}
     */
    @Override
    public DaffodilProcessor getProcessorFromGivenDfdlSchema(byte[] dfdlSchema) throws IOException, CompilationException {

        Path dfdlFilePath = Files.createTempFile("dfdlSchema", ".tmp");
        Files.write(dfdlFilePath, dfdlSchema);

        return DaffodilProcessorFactory.compileProcessor(dfdlFilePath.toFile());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DaffodilProcessor getProcessorFromDfdlReference(DfdlSchema dfdlSchema) throws IOException, CompilationException {

        if (dfdlSchemaDAO.isCompiled(dfdlSchema)) {
            try {
                return DaffodilProcessorFactory.reloadProcessor(dfdlSchemaDAO.getCompiledFile(dfdlSchema));
            } catch (ProcessorReloadException e) {
                LOG.warn("Unexpected error while reloading an existing compiled DFDL schema: " + e.getMessage());
                LOG.warn("Will try to recompile it");
            }
        }

        // Do not catch those errors. The method caller must know that the provider has failed to build processor
        DaffodilProcessor processor = DaffodilProcessorFactory.compileProcessor(dfdlSchemaDAO.getDfdlSchemaFile(dfdlSchema));

        try {
            processor.save(dfdlSchemaDAO.getCompiledFile(dfdlSchema));
        } catch (IOException e) {
            // Failing to save the processor does not block the transformation. So just put a warning to inform admins
            LOG.warn(e.getMessage());
        }
        return processor;
    }

}
