package net.ihe.gazelle.transformation.dao;

import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;

import javax.ejb.Local;
import java.io.File;
import java.util.List;

@Local
public interface DfdlSchemaDAO extends FilteredDAO<DfdlSchema> {

    List<DfdlSchema> getAll();

    List<DfdlSchema> getAvailable();

    List<DfdlSchema> getAvailableForAGivenObjectType(String objectTypeKeyword);

    DfdlSchema getById(Integer id);

    DfdlSchema getByKeyword(String keyword);

    DfdlSchema getByName(String name);

    DfdlSchema save(DfdlSchema dfdlSchema) throws EntityConstraintException;

    void delete(DfdlSchema dfdlSchema);

    boolean isKeywordAlreadyUsed(String keyword);

    File getDfdlSchemaFile(DfdlSchema dfdlSchema);

    File getCompiledFile(DfdlSchema dfdlSchema);

    boolean isCompiled(DfdlSchema dfdlSchema);
}
