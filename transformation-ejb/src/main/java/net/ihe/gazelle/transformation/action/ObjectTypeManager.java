package net.ihe.gazelle.transformation.action;


import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.transformation.dao.ObjectTypeDAO;
import net.ihe.gazelle.transformation.model.ObjectType;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

@Name("objectTypeManagerBean")
@Scope(ScopeType.PAGE)
public class ObjectTypeManager implements Serializable {

    private static final long serialVersionUID = 1L;

    @In
    private ObjectTypeDAO objectTypeDAO;

    private transient Filter<ObjectType> filter;
    private ObjectType selectedObjectType;

    public Filter<ObjectType> getFilter() {
        if (this.filter == null) {
            this.filter = objectTypeDAO.createFilter();
        }
        return this.filter;
    }

    public FilterDataModel<ObjectType> getAllObjectTypes() {
        return objectTypeDAO.getFiltered(getFilter());
    }

    public ObjectType getSelectedObjectType() {
        return selectedObjectType;
    }

    public void setSelectedObjectType(ObjectType selectedObjectType) {
        this.selectedObjectType = selectedObjectType;
    }

    public void initCreateObjectType() {
        this.selectedObjectType = new ObjectType();
    }

    public void getObjectTypeFromUrl() {
        // If there's an id in the URL, get it and load the selectedObjectType
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.containsKey("id")) {
            Integer objectTypeId = Integer.parseInt(urlParams.get("id"));
            selectedObjectType = objectTypeDAO.getById(objectTypeId);
        }
    }

    public String editObjectType(ObjectType objectType) {
        return "/objecttype/edit.seam?id=" + objectType.getId();
    }

    public void deleteSelectedObjectType() {
        String keyword = selectedObjectType.getKeyword();
        objectTypeDAO.delete(selectedObjectType);
        FacesMessages.instance().add(Severity.INFO, "Object type '{0}' deleted", keyword);
    }

    public boolean isObjectTypeDeletable(ObjectType objectType) {
        return !objectTypeDAO.isObjectTypeReferenced(objectType);
    }

    public String save() {
        try {
            selectedObjectType = objectTypeDAO.save(selectedObjectType);
            FacesMessages.instance().add(Severity.INFO, "The object type '{0}' has been successfully saved", selectedObjectType.getName());
            return "/objecttype/list.seam";
        } catch (EntityConstraintException e) {
            FacesMessages.instance().add(Severity.ERROR, "Cannot save the object type: {0}", e.getMessage());
            return null;
        }
    }

}
