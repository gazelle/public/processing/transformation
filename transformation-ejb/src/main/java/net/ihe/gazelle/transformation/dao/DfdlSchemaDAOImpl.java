package net.ihe.gazelle.transformation.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.model.DfdlSchemaQuery;
import net.ihe.gazelle.transformation.other.exception.EntityConstraintException;
import net.ihe.gazelle.transformation.other.preferences.ApplicationConfigurationDAO;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.io.File;
import java.util.List;
import java.util.Map;


@Stateless
@AutoCreate
@Name("dfdlSchemaDAO")
public class DfdlSchemaDAOImpl implements DfdlSchemaDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    @Override
    public List<DfdlSchema> getAll() {
        DfdlSchemaQuery query = new DfdlSchemaQuery(entityManager);
        return query.getList();
    }

    @Override
    public List<DfdlSchema> getAvailable() {
        DfdlSchemaQuery hq = new DfdlSchemaQuery(entityManager);
        hq.available().eq(true);
        return hq.getList();
    }

    @Override
    public List<DfdlSchema> getAvailableForAGivenObjectType(String objectTypeKeyword) {
        if (objectTypeKeyword == null || objectTypeKeyword.length() == 0) {
            return null;
        }
        DfdlSchemaQuery query = new DfdlSchemaQuery(entityManager);
        query.available().eq(true);
        query.objectType().keyword().eq(objectTypeKeyword);
        return query.getList();
    }

    @Override
    public DfdlSchema getById(Integer id) {
        if (id == null || id <= 0) {
            return null;
        }
        return entityManager.find(DfdlSchema.class, id);
    }


    @Override
    public DfdlSchema getByKeyword(String keyword) {
        if (keyword == null || keyword.length() == 0) {
            return null;
        }
        DfdlSchemaQuery query = new DfdlSchemaQuery(entityManager);
        query.keyword().eq(keyword);
        return query.getUniqueResult();
    }

    @Override
    public DfdlSchema getByName(String name) {
        if (name == null || name.length() == 0) {
            return null;
        }
        DfdlSchemaQuery query = new DfdlSchemaQuery(entityManager);
        query.name().eq(name);
        return query.getUniqueResult();
    }

    @Override
    public DfdlSchema save(DfdlSchema dfdlSchema) throws EntityConstraintException {
        if (dfdlSchema != null) {
            try {
                dfdlSchema = entityManager.merge(dfdlSchema);
                entityManager.flush();
            } catch (PersistenceException e) {
                if (e.getCause() instanceof ConstraintViolationException) {
                    throw new EntityConstraintException((ConstraintViolationException) e.getCause());
                } else {
                    throw e;
                }
            }
        }
        return dfdlSchema;
    }

    @Override
    public void delete(DfdlSchema dfdlSchema) {
        if (dfdlSchema != null) {
            dfdlSchema = entityManager.find(DfdlSchema.class, dfdlSchema.getId());
            entityManager.remove(dfdlSchema);
            entityManager.flush();
        }
    }

    @Override
    public boolean isKeywordAlreadyUsed(String keyword) {
        DfdlSchemaQuery DfdlSchemaQuery = new DfdlSchemaQuery(entityManager);
        DfdlSchemaQuery.keyword().eq(keyword);
        return DfdlSchemaQuery.getCount() != 0;
    }

    @Override
    public File getDfdlSchemaFile(DfdlSchema dfdlSchema) {
        if (dfdlSchema != null && dfdlSchema.getPath() != null && !dfdlSchema.getPath().isEmpty()) {
            return new File(applicationConfigurationDAO.getValue("bin_path") + "/" + dfdlSchema.getPath());
        } else {
            return null;
        }
    }

    @Override
    public File getCompiledFile(DfdlSchema dfdlSchema) {
        if (dfdlSchema != null && dfdlSchema.getPath() != null && !dfdlSchema.getPath().isEmpty()) {
            return new File(applicationConfigurationDAO.getValue("bin_path") + "/compilations/" + dfdlSchema.getPath() + ".compiled");
        } else {
            return null;
        }
    }

    @Override
    public boolean isCompiled(DfdlSchema dfdlSchema) {
        File file = getCompiledFile(dfdlSchema);
        return (!file.exists() || !file.isFile()) ? false : true;
    }

    @Override
    public Filter<DfdlSchema> createFilter() {
        return new Filter<DfdlSchema>(getHQLCriteriaForFilter());
    }

    @Override
    public FilterDataModel<DfdlSchema> getFiltered(Filter<DfdlSchema> filter) {
        return new FilterDataModel<DfdlSchema>(filter) {
            @Override
            protected Object getId(DfdlSchema dfdlSchema) {
                return dfdlSchema.getId();
            }
        };
    }

    protected HQLCriterionsForFilter<DfdlSchema> getHQLCriteriaForFilter() {
        DfdlSchemaQuery query = new DfdlSchemaQuery(entityManager);
        final HQLCriterionsForFilter<DfdlSchema> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("label", query.keyword());
        criteria.addPath("version", query.version());
        criteria.addPath("path", query.path());
        criteria.addPath("lastChanged", query.lastChanged());
        criteria.addPath("available", query.available());
        criteria.addPath("objectType", query.objectType().keyword());

        criteria.addQueryModifier(new QueryModifier<DfdlSchema>() {
            @Override
            public void modifyQuery(HQLQueryBuilder<DfdlSchema> hqlQueryBuilder, Map<String, Object> map) {
            }
        });

        return criteria;
    }
}
