package net.ihe.gazelle.transformation.other.exception;

import org.hibernate.exception.ConstraintViolationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EntityConstraintException extends Exception {

    private static final long serialVersionUID = 1044640342800681020L;

    private static final String UNIQUE_REGEX = "ERROR: duplicate key value violates unique constraint \".*\"$" +
            "\\s+Détail.: Key \\((\\w+)\\)=\\((\\w+)\\) already exists\\.";
    private static final Pattern uniquePattern = Pattern.compile(UNIQUE_REGEX, Pattern.MULTILINE);

    public EntityConstraintException(ConstraintViolationException e) {
        super(translateMessageForHuman(e));
    }

    private static String translateMessageForHuman(ConstraintViolationException e) {
        String originalMessage = e.getSQLException().getMessage();

        // Detect and translate Unique constraint violation
        Matcher uniqueMatcher = uniquePattern.matcher(originalMessage);
        if (uniqueMatcher.matches()) {
            return String.format("%s with value '%s' already exists", uniqueMatcher.group(1), uniqueMatcher.group(2));
        }

        // TODO Add notnull constraint violation detection

        // TODO Add foreignkey constraint violation detection

        return originalMessage;
    }

}
