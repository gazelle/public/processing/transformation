package net.ihe.gazelle.transformation.other.metainfservices;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;
import net.ihe.gazelle.preferences.PreferenceService;
import org.kohsuke.MetaInfServices;

import java.util.HashMap;
import java.util.Map;

@MetaInfServices(CSPPoliciesPreferences.class)
public class CSPPoliciesPreferencesProvider implements CSPPoliciesPreferences {

    @Override
    public boolean isContentPolicyActivated() {
        return Boolean.parseBoolean(PreferenceService.getString(PreferencesKey.SECURITY_POLICIES.getFriendlyName()));
    }

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        HashMap<String, String> headers = new HashMap();
        for (PreferencesKey preferencesKey : PreferencesKey.values()) {
            if (!preferencesKey.equals(PreferencesKey.SQL_INJECTION_FILTER_SWITCH)) {
                headers.put(preferencesKey.getFriendlyName(), PreferenceService.getString(preferencesKey.getFriendlyName()));
            }
        }
        return headers;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return Boolean.parseBoolean(PreferenceService.getString(PreferencesKey.SQL_INJECTION_FILTER_SWITCH.getFriendlyName()));
    }

}
