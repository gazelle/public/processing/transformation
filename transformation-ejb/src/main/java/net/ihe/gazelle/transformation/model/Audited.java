/*
 * Copyright 2008 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.transformation.model;

//JPA imports

import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


// Java/JBoss imports


/**
 * <b>Class Description :  </b>Audit Module<br><br>
 * This abstract class is intended to be extended by others classes, for the purpose of implementing
 * audit module. This module helps to keep informations about changes, these informations are stored
 * in the database.
 * <ul>
 * <li><b>lastChanged</b> : When the last change has been performed.</li>
 * <li><b>lastModifiedId</b> : Who did the last change (which user) </li>
 * </ul></br>
 * These attributes are mapped with the database, in the table corresponding to the associated object. <br>
 * <b>Example </b> : If an Actor object extends Audited, it will get these two additional attributes,
 * and two associated columns in its table. <br>
 *
 * @class Audited.java
 * @package net.ihe.gazelle.common.audit
 * @author Jean-Renan Chatel / INRIA Rennes IHE development Project
 * @see    > Jchatel@irisa.fr  -  http://www.ihe-europe.org
 * @version 1.0 - 2007, October 21
 */

@MappedSuperclass
public abstract class Audited implements Serializable {


    /**
     * Attribute lastChanged
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed")
    protected Date lastChanged;

    /**
     * Attribute lastModifierId
     */
    @Column(name = "last_modifier_id")
    protected String lastModifierId;


    /**
     * Getter used for the lastChanged attribute.
     *
     * @return lastChanged : Timestamp corresponding to the last change.
     */
    public Date getlastChanged() {
        return (Date) lastChanged.clone();
    }

    /**
     * Setter used for the lastChanged attribute.
     *
     * @param lastChanged : Timestamp corresponding to the last change.
     */
    public void setLastChanged(Date lastChanged) {
        this.lastChanged = (Date) lastChanged.clone();
    }


    /**
     * Getter used for the lastModifierId attribute.
     *
     * @return lastModifierId : Id of the user who performed the last change.
     */
    public String getLastModifierId() {
        return lastModifierId;
    }

    /**
     * Setter used for the lastModifierId attribute.
     *
     * @param lastModifierId : Id of the user who performed the last change.
     */
    public void setLastModifierId(String lastModifierId) {
        this.lastModifierId = lastModifierId;
    }


    /**
     * Init Audit informations.
     * Get the current time, find and set the user id logged in.
     */
    @PrePersist
    @PreUpdate
    public void init() {
        this.setLastChanged(new Date());
        if (Contexts.isApplicationContextActive()) {
            if (Identity.instance().isLoggedIn()) {
                this.setLastModifierId(Identity.instance().getCredentials().getUsername());
            }
        }
    }


}
