package net.ihe.gazelle.transformation.ws;

import net.ihe.gazelle.daffodil.DaffodilProcessor;
import net.ihe.gazelle.transformation.model.DfdlSchema;
import net.ihe.gazelle.transformation.service.CompilationException;

import java.io.IOException;

/**
 * Provide a temporary {@link DaffodilProcessor} from a given DFDL schema, or build one according to a reference to a DfdlSchema registered in the
 * application.
 *
 * @author ceoche
 */
public interface DaffodilProcessorProvider {

    /**
     * Get a live {@link DaffodilProcessor} from a given DFDL Schema. It might use an intermediate temporary file.
     *
     * @param dfdlSchema is the DFDL XSD Schema String (in bytes)
     * @return a {@link DaffodilProcessor} ready for transformation with the given DFDL schema
     * @throws IOException          in case of file access error
     * @throws CompilationException in case of schema compilation error. Use {@link CompilationException#getMessage()} to get compilation details
     */
    DaffodilProcessor getProcessorFromGivenDfdlSchema(byte[] dfdlSchema) throws IOException, CompilationException;

    /**
     * Get a {@link DaffodilProcessor} from a reference to a DfdlSchema registered in the application. It will either reload the saved processor if
     * already compiled or compile and save it if not.
     *
     * @param dfdlKeyword keyword to reference a registered DfdlSchema
     * @return a {@link DaffodilProcessor} ready for transformation
     * @throws IOException          in case of file access error while trying to compile the schema
     * @throws CompilationException in case of schema compilation error. Use {@link CompilationException#getMessage()} to get compilation details
     */
    DaffodilProcessor getProcessorFromDfdlReference(DfdlSchema dfdlSchema) throws IOException, CompilationException;

}
